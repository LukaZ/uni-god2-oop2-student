#ifndef DOMACITABLEMODEL_H
#define DOMACITABLEMODEL_H

#include "korisnikqsqlrelationaltablemodel.h"

class DomaciTableModel : public KorisnikQSqlRelationalTableModel
{
    public:
        //DomaciTableModel( const QList<Domaci> &data, QObject *parent  );
        DomaciTableModel(QObject *parent, QString table, int korisnikid);
        ~DomaciTableModel();
};

#endif // DOMACITABLEMODEL_H
