#pragma once
#include <QMainWindow>
#include <QtSql>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include <QString>
#include <QSqlRecord>
#include <QDebug>
#include <QSqlError>
#include <QStyle>
#include <QTableWidget>
#include <QIcon>
#include <QDialog>
#include <QVector>
#include <QSqlRelationalDelegate>
#include <QPoint>

#include "dodaj_ispit.h"
#include "dodajdomaci.h"
#include "dodajpredmet.h"
#include "loginform.h"
#include "database.h"

#include "domacitablemodel.h"
#include "ispittablemodel.h"
#include "predmettablemodel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        Database database; // klasa za upravljanje konekcije ka sqlite databazi

        void populateIspitiTableView(); // metoda za populisanje ispiti_tableView tabeview-a
        void populateDomaciTabelView();
        void populatePredmetiTableView();

        bool connectToDatabase();
        bool addIspit(noviIspit *ispit);
        bool addDomaci(NoviDomaci *domaci);
        bool addPredmet(NoviPredmet *predmet);

        void connectTableViewContextMenus();
        void contextDeleteRow(QString tableToUse, int dataEntryID);
        void refreshAllTables();
        void resizeTableViews();

        MainWindow(QWidget *parent = nullptr, LoginForm *form=nullptr); // konstruktor za glavni prozor
        ~MainWindow();

    public slots:
        void customMenuRequested(QPoint pos, QString tableToUse);
        void customMenuRequested_Domaci(QPoint pos);
        void customMenuRequested_Ispiti(QPoint pos);
        void customMenuRequested_Predmeti(QPoint pos);


    private slots:


        void on_dodajIspit_pushButton_clicked();

        void on_dodajDomaci_pushButton_clicked();

        void on_dodajPredmet_pushButton_clicked();

        void on_osveziIspite_pushButton_clicked();

        void on_osveziDomace_pushButton_clicked();

        void on_osveziPredmete_pushButton_clicked();

        void onLoginAttempt(QWidget *form, LoginCredentials *creds);


private:
        Ui::MainWindow *ui;

};
