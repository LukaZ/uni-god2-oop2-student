#include "user.h"

User::User()
{

}

void User::setActiveUser(int id, QString username)
{
    this->id = id;
    this->username = username;
}

int User::getUserID()
{
    return this->id;
}

QString User::getUsername()
{
    return this->username;
}
