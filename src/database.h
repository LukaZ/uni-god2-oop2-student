#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSql>
#include <QString>
#include <QSqlError>
#include <QVariant>
#include <QDebug>
#include <QSqlField>
#include <QSqlDriver>
#include <QDate>

#include "user.h"

struct QuerryResult
{
    bool success;
    QSqlQuery qrr;
    QString err;
};

class Database
{
    private:
        const char* SQL_SELECT_DOMACI = "SELECT * FROM domaci WHERE korisnikid = ?";
        const char* SQL_DELETE_DOMACI = "DELETE FROM domaci WHERE id = ?";

        const char* SQL_SELECT_ISPITI = "SELECT * FROM ispiti WHERE korisnikid = ?";
        const char* SQL_DELETE_ISPITI = "DELETE FROM ispiti WHERE id = ?";

        const char* SQL_SELECT_PREDMETI = "SELECT * FROM predmeti";
        const char* SQL_ADD_PREDMET = "INSERT INTO predmeti(akronim, naziv, semestar, profesor, asistent) VALUES (?, ?, ?, ?, ?)";
        const char* SQL_DELETE_PREDMET = "DELETE FROM predmeti WHERE id = ?";

        QSqlDatabase database; // klasa za upravljanje konekcije ka sqlite databazi
        User currentUser;
    public slots:
        QuerryResult handleDeleteByID(QString tableToDeleteFrom, int id);
    public:
        Database();

        User *getCurrentUser();
        QSqlDatabase *getDatabase();

        QuerryResult attemptUserAuth(QString username, QString password);
        QuerryResult getUserIspiti(int userID);
        QuerryResult getUserDomaci(int userID);
        QuerryResult getPredmeti();

        QuerryResult addDomaci(QString predmet, QString opis, QDate rok);
        QuerryResult addIspit(QString predmet, QString opis, QDate rok);
        QuerryResult addPredmet(QString akronim, QString naziv, int semestar, QString profesor, QString asistent);

        QuerryResult deleteDomaci(int id);
        QuerryResult deleteIspit(int id);
        QuerryResult deletePredmet(int id);

        QString getLastExecutedQuery(const QSqlQuery& query);

        bool connect(QString databaseType, QString ip, QString databaseName, QString username, QString password, int port=3306);
        void close();
};


