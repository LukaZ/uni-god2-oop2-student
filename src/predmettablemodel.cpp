#include "predmettablemodel.h"

PredmetTableModel::PredmetTableModel(QObject *parent, QString table, int korisnikid) : KorisnikQSqlRelationalTableModel(parent, table, korisnikid)
{
    // Predmeti su nezavisni od korisnika samim time nemamo QSqlRelation
    this->select();
}


PredmetTableModel::~PredmetTableModel()
{

}
