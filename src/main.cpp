#include "mainwindow.h"
#include "loginform.h"

#include <QApplication>
#include <QScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LoginForm login;
    MainWindow w (nullptr, &login);

    login.ShowLogin();

    return a.exec();
}
