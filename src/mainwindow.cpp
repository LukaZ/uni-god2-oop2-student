#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>

bool MainWindow::connectToDatabase(){
    database = Database();
    bool res = database.connect("QMYSQL","0.0.0.0", "student", "root", "97VrXg8CNoW1eB");

    if ( !res )
    { // da li nesto idalje koristi databazu ?
        QMessageBox::critical(this, "Greska!", "Nismo uspeli da se konektujemo ka bazi\n Neuspesna konekcija!");
    }

    return true;
}

MainWindow::MainWindow(QWidget *parent, LoginForm *login):QMainWindow(parent), ui(new Ui::MainWindow) { // Ulazna tacka

    ui->setupUi(this);

    this->connectToDatabase();

    connect(login, &LoginForm::loginAttempt, this, &MainWindow::onLoginAttempt);

}

void MainWindow::connectTableViewContextMenus()
{
//    /SIGNAL(customContextMenuRequested(QPoint))
    ui->domaci_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->predmeti_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->ispiti_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->domaci_tableView, SIGNAL(customContextMenuRequested(QPoint)) , SLOT(customMenuRequested_Domaci(QPoint)));
    connect(ui->ispiti_tableView, SIGNAL(customContextMenuRequested(QPoint)) , SLOT(customMenuRequested_Ispiti(QPoint)));
    connect(ui->predmeti_tableView, SIGNAL(customContextMenuRequested(QPoint)) , SLOT(customMenuRequested_Predmeti(QPoint)));
}

void MainWindow::refreshAllTables()
{
    this->populateIspitiTableView();
    this->populateDomaciTabelView();
    this->populatePredmetiTableView();
}

void MainWindow::onLoginAttempt(QWidget *form, LoginCredentials *creds)
{
    QuerryResult res = database.attemptUserAuth(creds->username, creds->password);
    QString querry = database.getLastExecutedQuery(res.qrr);
    if (res.success)
    {
        form->close();
        this->show();

        this->refreshAllTables();

        this->connectTableViewContextMenus();
    }
    else
    {
        QMessageBox::critical(this, "Greska prilikom autentifikacije", res.err);
    }

}

void MainWindow::contextDeleteRow(QString tableToUse, int dataEntryID)
{
    if (QMessageBox::question(this, "Potvrdite akciju", QString("Da li ste sigurni da zelite da izbrisete zapis sa id %1").arg(dataEntryID)) == QMessageBox:: Yes)
    {
        QuerryResult res = this->database.handleDeleteByID(tableToUse, dataEntryID);
        if (res.success)
        {
            QMessageBox::information(this, "Uspesno izbrisano", QString("Uspesno izbrisan zapis sa ID %1").arg(dataEntryID));
            refreshAllTables();
        }
        else
        {
            QMessageBox::critical(this, "Greska", "Greska prilikom brisanja zapisa; "+res.err);
        }
    }

}

void MainWindow::customMenuRequested_Domaci(QPoint pos)
{
    customMenuRequested(pos, "domaci");
}

void MainWindow::customMenuRequested_Ispiti(QPoint pos)
{
    customMenuRequested(pos, "ispiti");
}

void MainWindow::customMenuRequested_Predmeti(QPoint pos)
{
    customMenuRequested(pos, "predmeti");
}

void MainWindow::customMenuRequested(QPoint pos, QString tableToUse)
{

    QTableView *table = static_cast<QTableView*>(sender());
    QModelIndex index=table->indexAt(pos);
    int dataEntryID = table->model()->data(table->model()->index(index.row(), 0)).toInt();

    QMenu *menu=new QMenu(this);

    QAction *deleteAction = new QAction("Izbrisi zapis", this);
    deleteAction->setShortcut(QKeySequence::Delete);
    connect(deleteAction, &QAction::triggered, [this, dataEntryID, tableToUse](){ contextDeleteRow(tableToUse, dataEntryID); });
    menu->addAction(deleteAction);

    //menu->addAction(new QAction("Action 2", this));
    //menu->addAction(new QAction("Action 3", this));
    menu->popup(table->viewport()->mapToGlobal(pos));
}

void MainWindow::resizeTableViews()
{
    ui->domaci_tableView->resizeColumnsToContents();
    ui->ispiti_tableView->resizeColumnsToContents();
    ui->predmeti_tableView->resizeColumnsToContents();
}


void MainWindow::populatePredmetiTableView()
{
    QuerryResult res = database.getPredmeti();
    QString querry = database.getLastExecutedQuery(res.qrr);
    if (res.success)
    {
        PredmetTableModel *model = new PredmetTableModel(ui->predmeti_tableView, "predmeti", -1);
        ui->predmeti_tableView->setModel(model);

        this->resizeTableViews();
    }
    else
    {
        QMessageBox::critical(this, "Greska prilikom popunjavanja podataka za ispite", res.err);
    }
}

void MainWindow::populateIspitiTableView()
{
    QuerryResult res = database.getUserIspiti(this->database.getCurrentUser()->getUserID());
    QString querry = database.getLastExecutedQuery(res.qrr);
    if (res.success)
    {
        IspitTableModel *model = new IspitTableModel(ui->ispiti_tableView, "ispiti", this->database.getCurrentUser()->getUserID());
        ui->ispiti_tableView->setModel(model);
        ui->ispiti_tableView->setItemDelegate(new QSqlRelationalDelegate(ui->ispiti_tableView)); // omogucava da imamo dropdown combobox za polja

        this->resizeTableViews(); // moramo ponovo podesiti velicine
    }
    else
    {
        QMessageBox::critical(this, "Greska prilikom dobavljanja domaceg", res.err);
    }
}

void MainWindow::populateDomaciTabelView()
{
    QuerryResult res = database.getUserDomaci(this->database.getCurrentUser()->getUserID());
    QString querry = database.getLastExecutedQuery(res.qrr);
    if (res.success)
    {
        DomaciTableModel *model = new DomaciTableModel(ui->domaci_tableView, "domaci", this->database.getCurrentUser()->getUserID());
        ui->domaci_tableView->setModel(model);
        ui->domaci_tableView->setItemDelegate(new QSqlRelationalDelegate(ui->domaci_tableView));// omogucava da imamo dropdown combobox za polja

        this->resizeTableViews(); // moramo ponovo podesiti velicine
    }
    else
    {
        QMessageBox::critical(this, "Greska prilikom dobavljanja domaceg", res.err);
    }
}

bool MainWindow::addIspit(noviIspit *ispit)
{
    QuerryResult res = database.addIspit(ispit->predmet, ispit->opis, ispit->rok);

    if ( !res.success ){
        QMessageBox::critical( this,"Greska pri izvrsavanju SQL upita", "Greska: \n" + res.err );
        return false;
    }
    else
    {
        return true;
    }

    return false;
}

bool MainWindow::addDomaci(NoviDomaci *domaci)
{
    QuerryResult res = database.addDomaci(domaci->predmet, domaci->opis, domaci->rok);

    if ( !res.success ){
        QMessageBox::critical( this,"Greska pri izvrsavanju SQL upita", "Greska: \n" + res.err );
        return false;
    }
    else
    {
        return true;
    }

    return false;
}

bool MainWindow::addPredmet(NoviPredmet *predmet)
{
    QuerryResult res = database.addPredmet(predmet->akronim, predmet->naziv, predmet->semestar, predmet->profesor, predmet->asistent);

    if ( !res.success ){
        QMessageBox::critical( this,"Greska pri izvrsavanju SQL upita", "Greska: \n" + res.err );
        return false;
    }
    else
    {
        return true;
    }

    return false;
}

void MainWindow::on_dodajIspit_pushButton_clicked()
{
    dodaj_ispit *di = new dodaj_ispit;
    di->setModal(true);
    if ( di->exec() == QDialog::Accepted )
    {

        //noviIspit res = di->getData();
        if ( this->addIspit(di->getData()) )
        {
            QMessageBox::information(this, "Uspesno izvrseno!", "Uspesno dodato u databazu");
        }

    }
}

void MainWindow::on_dodajDomaci_pushButton_clicked()
{
    DodajDomaci dd(this);
    dd.setModal(true);
    if ( dd.exec() == QDialog::Accepted )
    {
        if ( this->addDomaci( dd.getData()) )
        {
            QMessageBox::information(this, "Uspesno izvrseno!", "Uspesno dodato u databazu");
        }

    }
}


void MainWindow::on_dodajPredmet_pushButton_clicked()
{
    DodajPredmet dp(this);
    dp.setModal(true);
    if (dp.exec() == QDialog::Accepted)
    {
        if (this->addPredmet(dp.getData()))
        {
            QMessageBox::information(this, "Uspesno izvrseno!", "Uspesno dodato u databazu");
        }
    }
}


void MainWindow::on_osveziIspite_pushButton_clicked()
{
    this->populateIspitiTableView();
}

void MainWindow::on_osveziDomace_pushButton_clicked()
{
    this->populateDomaciTabelView();
}

void MainWindow::on_osveziPredmete_pushButton_clicked()
{
    this->populatePredmetiTableView();
}

MainWindow::~MainWindow()
{
    database.close();
    delete ui;
}


