#include "dodajpredmet.h"
#include "ui_dodajpredmet.h"


DodajPredmet::DodajPredmet(QWidget *parent) : QDialog(parent), ui(new Ui::DodajPredmet)
{
    ui->setupUi(this);
}


DodajPredmet::~DodajPredmet()
{
    delete ui;
}

NoviPredmet* DodajPredmet::getData()
{
    NoviPredmet *np = new NoviPredmet;
    np->akronim = ui->akronim_lineEdit->text();
    np->naziv = ui->naziv_lineEdit->text();
    np->semestar = ui->semestar_spinBox->value();
    np->profesor = ui->profesor_lineEdit->text();
    np->asistent = ui->asistent_lineEdit->text();

    return np;
}

void DodajPredmet::on_finish_pushButton_clicked()
{
    accept();
}

void DodajPredmet::on_cancel_pushButton_clicked()
{
    reject();
}
