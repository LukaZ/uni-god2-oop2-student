#ifndef PREDMETTABLEMODEL_H
#define PREDMETTABLEMODEL_H

#include "korisnikqsqlrelationaltablemodel.h"

class PredmetTableModel : public KorisnikQSqlRelationalTableModel
{
    public:
        PredmetTableModel(QObject *parent, QString table, int korisnikid);
        ~PredmetTableModel();
};

#endif // PREDMETTABLEMODEL_H
