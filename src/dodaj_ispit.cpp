#include "dodaj_ispit.h"
#include "ui_dodaj_ispit.h"



dodaj_ispit::dodaj_ispit(QWidget *parent) : QDialog(parent),ui(new Ui::dodaj_ispit)
{
    ui->setupUi(this);
}

noviIspit* dodaj_ispit::getData()
{
    noviIspit *ns = new noviIspit();

    ns->predmet = ui->predmet_lineEdit->text();
    ns->opis = ui->opis_lineEdit->text();
    ns->rok = ui->rokKdatecombobox->date();
    return ns;
}

void dodaj_ispit::on_dodaj_buttonBox_accepted()
{
    accept();
}

dodaj_ispit::~dodaj_ispit()
{
    delete ui;
}
