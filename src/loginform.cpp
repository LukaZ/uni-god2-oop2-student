#include "loginform.h"
#include "ui_loginform.h"

LoginForm::LoginForm(QWidget *parent) :QDialog(parent),ui(new Ui::LoginForm)
{
    ui->setupUi(this);

    QPixmap pic(":/assets/images/images/university-png--512.png");
    ui->welcomeLabel->setPixmap(pic);
    ui->welcomeLabel->setScaledContents(true);
}

void LoginForm::ShowLogin()
{
    this->show();
}

LoginForm::~LoginForm()
{
    delete ui;
}

void LoginForm::on_loginButton_clicked()
{
    QString username = ui->usernameLineEdit->text();
    QString password = ui->passwordLineEdit->text();

    LoginCredentials creds = {username, password};
    emit loginAttempt(this , &creds);



}
