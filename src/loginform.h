#ifndef LOGINFORM_H
#define LOGINFORM_H

#include <QDialog>
#include <QPixmap>

namespace Ui {
    class LoginForm;
}

struct LoginCredentials // C++ ne podrzava vracanje 2 vrednosti iz jedne funkcije
{
    QString username;
    QString password;
};

class LoginForm : public QDialog
{
    Q_OBJECT

    public:
        explicit LoginForm(QWidget *parent = nullptr);
        void ShowLogin();
        ~LoginForm();

    signals:
         void loginAttempt(QWidget *loginForm, LoginCredentials *creds);

    public slots:
        void on_loginButton_clicked();

    private:
        Ui::LoginForm *ui;
};

#endif // LOGINFORM_H
