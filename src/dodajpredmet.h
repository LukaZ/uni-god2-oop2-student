#ifndef DODAJPREDMET_H
#define DODAJPREDMET_H

#include <QDialog>

namespace Ui {
    class DodajPredmet;
}

struct NoviPredmet{
    QString akronim;
    QString naziv;
    int semestar;
    QString profesor;
    QString asistent;
};

class DodajPredmet : public QDialog
{
    Q_OBJECT

    public:
        explicit DodajPredmet(QWidget *parent = nullptr);
        ~DodajPredmet();
        NoviPredmet* getData();

    private slots:
        void on_finish_pushButton_clicked();

        void on_cancel_pushButton_clicked();

    private:
                Ui::DodajPredmet *ui;
};

#endif // DODAJPREDMET_H
