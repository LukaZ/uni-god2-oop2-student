#ifndef KORISNIKQSQLRELATIONALTABLEMODEL_H
#define KORISNIKQSQLRELATIONALTABLEMODEL_H

#include <QSqlRelationalTableModel>

// ZA UPOTREBU U POPULISANJU PODATAKA ZA ISPIT I DOMACI

class KorisnikQSqlRelationalTableModel :  public QSqlRelationalTableModel
{
    private:
        int korisnikid;
    public:
        KorisnikQSqlRelationalTableModel(QObject *parent, QString table, int korisnikid);
        ~KorisnikQSqlRelationalTableModel();
        void setKorisnikID(int id);

};

#endif // KORISNIKQSQLRELATIONALTABLEMODEL_H
