#pragma once
#include <QString>
#include <QMessageBox>
#include <QVariant>
#include <QSqlQuery>
#include <QSqlError>


#include "loginform.h"

class User
{
    private:
        int id;
        QString username;

    public:
        User();
        void setActiveUser(int id, QString username);
        int getUserID();
        QString getUsername();
        // bool attemptToAuth(QWidget *parrent, LoginCredentials *creds, Database *db);
};


