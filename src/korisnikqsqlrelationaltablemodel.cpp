#include "korisnikqsqlrelationaltablemodel.h"

KorisnikQSqlRelationalTableModel::KorisnikQSqlRelationalTableModel(
        QObject *parent, QString table, int korisnikid) : QSqlRelationalTableModel ( parent )
{
    this->setTable(table);
    this->korisnikid = korisnikid;
    if (korisnikid >= 0) // da li smo mu zadali korisnikid ?
    {
        this->setFilter(QString("korisnikid = %1").arg(this->korisnikid));
    }
    this->setEditStrategy(QSqlTableModel::OnFieldChange);
}

KorisnikQSqlRelationalTableModel::~KorisnikQSqlRelationalTableModel()
{

}

void KorisnikQSqlRelationalTableModel::setKorisnikID(int id)
{
    this->korisnikid = id;
}

