#ifndef DODAJ_ISPIT_H
#define DODAJ_ISPIT_H

#include <QDialog>
#include <QVector> // QT implementacija <vector> , https://doc.qt.io/qt-5/qvector.html
#include <QDate>

namespace Ui {
    class dodaj_ispit;
}

struct noviIspit{
    QString predmet;
    QString opis;
    QDate rok;
};

class dodaj_ispit : public QDialog
{
    Q_OBJECT

    public:
        explicit dodaj_ispit(QWidget *parrent = nullptr);
        ~dodaj_ispit();
        QString id;
        QString predmet;
        QString opis;
        QString datum;

        noviIspit* getData();

    private slots:
            void on_dodaj_buttonBox_accepted();

    private:
            Ui::dodaj_ispit *ui;
};

#endif // DODAJ_ISPIT_H
