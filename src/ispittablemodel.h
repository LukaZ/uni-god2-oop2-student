#ifndef ISPITTABLEMODEL_H
#define ISPITTABLEMODEL_H

#include "korisnikqsqlrelationaltablemodel.h"

class IspitTableModel : public KorisnikQSqlRelationalTableModel
{
    public:
        IspitTableModel(QObject *parent, QString table, int korisnikid);
        ~IspitTableModel();
};

#endif // ISPITTABLEMODEL_H
