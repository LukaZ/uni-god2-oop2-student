#include "dodajdomaci.h"
#include "ui_dodajdomaci.h"

DodajDomaci::DodajDomaci(QWidget *parent) : QDialog(parent), ui(new Ui::DodajDomaci)
{
    ui->setupUi(this);
}

NoviDomaci* DodajDomaci::getData()
{
    NoviDomaci *nd = new NoviDomaci();
    nd->predmet = ui->predmet_lineEdit->text();
    nd->opis = ui->opis_lineEdit->text();
    nd->rok = ui->rokKdatecombobox->date();
    return nd;
}

DodajDomaci::~DodajDomaci()
{
    delete ui;
}

void DodajDomaci::on_buttonBox_accepted()
{
    accept();
}
