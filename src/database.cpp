#include "database.h"


Database::Database()
{

}

bool Database::connect(QString databaseType, QString ip, QString databaseName, QString username, QString password, int port)
{
    database = QSqlDatabase::addDatabase(databaseType);
    database.setHostName(ip);
    database.setDatabaseName(databaseName);
    database.setUserName(username);
    database.setPassword(password);
    database.setPort(port);

    return database.open();
}

QSqlDatabase *Database::getDatabase()
{
    return &this->database;
}

QuerryResult Database::handleDeleteByID(QString tableToDeleteFrom, int id)
{
    QuerryResult res;
    if (tableToDeleteFrom == "domaci")
        res = this->deleteDomaci(id);
    else if (tableToDeleteFrom == "ispiti")
        res = this->deleteIspit(id);
    else if (tableToDeleteFrom == "predmeti")
        res = this->deletePredmet(id);

    return res;
}

QuerryResult Database::deleteDomaci(int id)
{
    QSqlQuery qry;
    qry.prepare(SQL_DELETE_DOMACI);
    qry.addBindValue(id);
    if ( !qry.exec())
    {
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}

QuerryResult Database::deleteIspit(int id)
{
    QSqlQuery qry;
    qry.prepare(SQL_DELETE_ISPITI);
    qry.addBindValue(id);
    if ( !qry.exec())
    {
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}

QuerryResult Database::deletePredmet(int id)
{
    QSqlQuery qry;
    qry.prepare(SQL_DELETE_PREDMET);
    qry.addBindValue(id);
    if ( !qry.exec())
    {
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}


QuerryResult Database::addPredmet(QString akronim, QString naziv, int semestar, QString profesor, QString asistent)
{
    if ( !database.isValid() ) { qDebug() << QString("Neispravna konekcija!"); }
    database.open();

    QSqlQuery qry;
    qry.prepare(SQL_ADD_PREDMET);
    qry.addBindValue(akronim);
    qry.addBindValue(naziv);
    qry.addBindValue(semestar);
    qry.addBindValue(profesor);
    qry.addBindValue(asistent);

    if ( !qry.exec() ){
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}


QuerryResult Database::addIspit(QString predmet, QString opis, QDate rok)
{
    if ( !database.isValid() ) { qDebug() << QString("Neispravna konekcija!"); }
    database.open();

    QSqlQuery qry;
    qry.prepare("INSERT INTO ispiti"
                "(predmet, opis, rok, korisnikid)"
                "VALUES"
                "(?, ?, ?, ? )");


    qry.addBindValue(predmet);
    qry.addBindValue(opis);
    qry.addBindValue(rok);
    qry.addBindValue(this->currentUser.getUserID());

    if ( !qry.exec() ){
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}

QuerryResult Database::addDomaci(QString predmet, QString opis, QDate rok )
{
    if ( !database.isValid() ) { qDebug() << QString("Neispravna konekcija!"); }
    database.open();

    QSqlQuery qry;
    qry.prepare("INSERT INTO domaci"
                "(predmet, opis, rok, korisnikid)"
                "VALUES"
                "(?, ?, ?, ?)");

    qry.addBindValue(predmet);
    qry.addBindValue(opis);
    qry.addBindValue(rok);
    qry.addBindValue(this->currentUser.getUserID());

    if ( !qry.exec() ){
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}

QuerryResult Database::attemptUserAuth(QString username, QString password)
{
    if ( !database.isValid() ) { qDebug() << QString("Neispravna konekcija!"); }
    database.open();

    QSqlQuery qry(database);
    qry.prepare("SELECT id FROM korisnici WHERE korisnicko_ime = ? AND lozinka = ?");

    qry.addBindValue(username);
    qry.addBindValue(password);

    if ( !qry.exec())
    {

        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else if ( !qry.next() )
    {
        QuerryResult res = { false, qry , "No records found" };
        return res;
    }
    else
    {
        int userID = qry.value(0).toInt();

        this->currentUser.setActiveUser(userID, username);
        QuerryResult res = { true, qry , "" };
        return res;
    }
}


QuerryResult Database::getPredmeti()
{
    database.open();

    QSqlQuery qry;
    qry.prepare(SQL_SELECT_PREDMETI);
    if ( !qry.exec())
    {
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}


QuerryResult Database::getUserDomaci(int userID)
{
    database.open();

    QSqlQuery qry;
    qry.prepare(SQL_SELECT_DOMACI);//"SELECT * FROM domaci WHERE korisnikid = ?");
    qry.addBindValue(userID);
    if ( !qry.exec())
    {

        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}


QuerryResult Database::getUserIspiti(int userID)
{
    database.open();

    QSqlQuery qry;
    qry.prepare("SELECT * FROM ispiti WHERE korisnikid = ?");
    qry.addBindValue(userID);
    if ( !qry.exec())
    {
        QuerryResult res = { false, qry ,qry.lastError().text() };
        return res;
    }
    else
    {
        QuerryResult res = { true, qry , "" };
        return res;
    }
}


User* Database::getCurrentUser()
{
    return &currentUser;
}

QString Database::getLastExecutedQuery(const QSqlQuery& query)
{
    QString sql = query.executedQuery();
    int nbBindValues = query.boundValues().size();

    for(int i = 0, j = 0; j < nbBindValues;)
    {
        int s = sql.indexOf(QLatin1Char('\''), i);
        i = sql.indexOf(QLatin1Char('?'), i);
        if (i < 1)
        {
            break;
        }

        if(s < i && s > 0)
        {
            i = sql.indexOf(QLatin1Char('\''), s + 1) + 1;
            if(i < 2)
            {
                break;
            }
        }
        else
        {
            const QVariant &var = query.boundValue(j);
            QSqlField field(QLatin1String(""), var.type());
            if (var.isNull())
            {
                field.clear();
            }
            else
            {
                field.setValue(var);
            }
            QString formatV = query.driver()->formatValue(field);
            sql.replace(i, 1, formatV);
            i += formatV.length();
            ++j;
        }
    }

    return sql;
}


void Database::close()
{
    database.close();
}
