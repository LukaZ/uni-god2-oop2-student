#ifndef DODAJDOMACI_H
#define DODAJDOMACI_H

#include <QDialog>
#include <QDate>
#include <QPushButton>

namespace Ui {
    class DodajDomaci;
}

struct NoviDomaci{
    QString predmet;
    QString opis;
    QDate rok;
};

class DodajDomaci : public QDialog
{
    Q_OBJECT

    public:
        explicit DodajDomaci(QWidget *parent = nullptr);
        ~DodajDomaci();
        NoviDomaci* getData();

    private slots:
            void on_buttonBox_accepted();

    private:
            Ui::DodajDomaci *ui;
};

#endif // DODAJDOMACI_H
