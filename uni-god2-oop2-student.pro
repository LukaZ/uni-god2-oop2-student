QT       += core gui sql widgets KWidgetsAddons

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 console debug


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    /usr/include/KF5

SOURCES += \
    src/database.cpp \
    src/dodajpredmet.cpp \
    src/domacitablemodel.cpp \
    src/ispittablemodel.cpp \
    src/korisnikqsqlrelationaltablemodel.cpp \
    src/loginform.cpp \
    src/dodaj_ispit.cpp \
    src/dodajdomaci.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/predmettablemodel.cpp \
    src/user.cpp

#HEADERS += $$files(/src/*.h, true)

HEADERS += \
    src/database.h \
    src/dodajpredmet.h \
    src/domacitablemodel.h \
    src/ispittablemodel.h \
    src/korisnikqsqlrelationaltablemodel.h \
    src/loginform.h \
    src/dodaj_ispit.h \
    src/dodajdomaci.h \
    src/mainwindow.h \
    src/predmettablemodel.h \
    src/user.h

#FORMS +=  $$files(/forms/*.ui, true)

FORMS += \
    forms/dodaj_ispit.ui \
    forms/dodajdomaci.ui \
    forms/loginform.ui \
    forms/mainwindow.ui \
    forms/dodajpredmet.ui

release: DESTDIR = bin/release
debug:   DESTDIR = bin/debug

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    data/assets/assets.qrc
