# Objasnjenje 

Jednostavni nacin za kreiranje lokalne instance MySQL databaze i njena populacija dummy podataka ( iz init.sql fajla ) bez potrebe da se instalira MySQL i MySQL Workbench i dodatno konfigurisanje tih programa.

Prilikom zavrsetka docker-compose moze se pristupiti adminer-u na 
http://localhost:8080/dashboard (username: root, password: 97VrXg8CNoW1eB) gde se moze raditi administracija nad bazom kao i neka osnovna manipulacija podataka ( ne koristiti u production ).

Ovaj specifican docker-compose.yml takodje se moze konfigurisati da ima perzistenciju podataka ali nebi verovao tome iz licnog iskustva, preporuceno je koristiti nezavisnu virtuelnu masinu sa MySQL/PostgreSQL ukoliko se trazi perzistencija podataka.

# Dependencies
docker

docker-compose

# Konfiguracija:
Editovati docker-compose.yml

# Upotreba:
```
$ ./init_docker_mysql.sh 
```
Potom selektovati zeljene opcije