CREATE DATABASE student;
USE student;


DROP TABLE IF EXISTS korisnici;
CREATE TABLE korisnici (
    id INT(16) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
    korisnicko_ime VARCHAR(60) NOT NULL,
    email VARCHAR(128) NOT NULL,
    lozinka VARCHAR(90)
) ENGINE='InnoDB';

DROP TABLE IF EXISTS predmeti;
CREATE TABLE predmeti (
    id INT(16) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
    akronim VARCHAR(24) NOT NULL,
    naziv VARCHAR(60) NOT NULL,
    semestar INT(16) NOT NULL,
    profesor VARCHAR(90) NOT NULL,
    asistent VARCHAR(256)
) ENGINE='InnoDB';

DROP TABLE IF EXISTS domaci;
CREATE TABLE domaci(
    id INT(16) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
    predmetid INT(16) unsigned NOT NULL,
    opis VARCHAR(90),
    rok DATE NOT NULL,
    korisnikid INT(16) unsigned NOT NULL,
    FOREIGN KEY (korisnikid) REFERENCES korisnici(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (predmetid) REFERENCES predmeti(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE='InnoDB';


DROP TABLE IF EXISTS ispiti;
CREATE TABLE ispiti(
    id INT(16) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
    predmetid INT(16) unsigned NOT NULL,
    opis VARCHAR(90),
    rok DATE NOT NULL,
    korisnikid INT(16) unsigned NOT NULL,
    FOREIGN KEY (korisnikid) REFERENCES korisnici(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (predmetid) REFERENCES predmeti(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE='InnoDB';


/* GENERISANJE PODATAKA */ 

INSERT INTO korisnici( korisnicko_ime, email, lozinka) VALUES ('luka', 'nobody@nowhere.ch', '123');
INSERT INTO korisnici( korisnicko_ime, email, lozinka) VALUES ('pera', 'pera@nowhere.ch', '321');


INSERT INTO predmeti( akronim, naziv, semestar, profesor, asistent ) VALUES ( 'OIS1OP2', 'Objektno orijentisano programiranje 2', 2, 'Djordje Obradovic', 'Ivan Radosavljevic' );
INSERT INTO predmeti( akronim, naziv, semestar, profesor, asistent ) VALUES ( 'OIS1MAT', 'Matematika', 1, 'Petar Peric', 'Marko Markovic');
INSERT INTO predmeti( akronim, naziv, semestar, profesor, asistent ) VALUES ( 'OIS1DKM', 'Digitalna Kriminalistika', 4, 'Maksim Maksimovic', 'Igor Igorevic');
INSERT INTO predmeti( akronim, naziv, semestar, profesor, asistent ) VALUES ( 'OIS1VIT', 'Vestacka Inteligencija 1', 6, 'Bosiljak Origano', 'Biber Vegeta');
INSERT INTO predmeti( akronim, naziv, semestar, profesor, asistent ) VALUES ( 'OIS1MAT', 'Matematika', 8, 'Hasim Hasidovic', 'Mehmed Kostic');


INSERT INTO domaci( predmetid, opis, rok, korisnikid) VALUES (1,'Zavrsiti predmetni projekat','2020-07-16',1);
INSERT INTO domaci( predmetid, opis, rok, korisnikid) VALUES (3,'Zavrsiti model za neuron mrezu','2020-02-01',1);
INSERT INTO domaci( predmetid, opis, rok, korisnikid) VALUES (2,'Zavrsiti izvestaj za Q2/2020','2020-03-25',1);

INSERT INTO ispiti( predmetid, opis, rok, korisnikid) VALUES (3,'Ispit vezan za AI', '2020-09-01', 1);
/*
INSERT INTO domaci( predmet, opis, rok, korisnikid) VALUES ('Matematika','Zavrsiti nacrt','2020-09-01',1);
INSERT INTO domaci( predmet, opis, rok, korisnikid) VALUES ('Kriptologija 1','Zavrsiti prezentaciju za AES256','2020-09-01',1);
INSERT INTO domaci( predmet, opis, rok, korisnikid) VALUES ('Kriminalistika 1','Zavrsiti izvestaj za Q1/2019','2020-09-01',2);
INSERT INTO domaci( predmet, opis, rok, korisnikid) VALUES ('Matematika','Ponovo uciti za ispit','2020-09-01',2);
INSERT INTO domaci( predmet, opis, rok, korisnikid) VALUES ('Vestacka Inteligencija','Novi algoritam uraditi','2020-09-01',2);

INSERT INTO ispiti( predmet, opis, rok, korisnikid) VALUES ('Biologija','Ispit vezan za celije', '2020-09-01', 1);
INSERT INTO ispiti( predmet, opis, rok, korisnikid) VALUES ('Racunarske Mreze 2','Osnovne konfiguracije mreze' , '2020-03-29', 1);
INSERT INTO ispiti( predmet, opis, rok, korisnikid) VALUES ('OpenCV 2 Projekat','Detekcija lica i generisanje jedinstvenog ID-ja' ,'2020-06-15', 1);
INSERT INTO ispiti( predmet, opis, rok, korisnikid) VALUES ('Vestacka Inteligencija','Osnovni AI za igricu' ,'2020-09-07', 2);
*/









