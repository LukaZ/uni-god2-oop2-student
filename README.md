
```mermaid
    classDiagram
        KorisnikQSqlRelationalTableModel ..|> QSqlRelationalTableModel : implementira
        
        MainWindow --|> QMainWindow : nasl
        MainWindow --o User : agre
        MainWindow --> dodaj_ispit : asc
        MainWindow --> DodajDomaci : asc
        MainWindow --> DodajPredmet : asc
        MainWindow -- Database : dir. asc
        MainWindow --> IspitTableModel
        MainWindow --> DomaciTableModel
        MainWindow --> PredmetTableModel
        MainWindow -- LoginForm : dir. asc

        LoginForm --|> QDialog : nasl
        LoginForm --o LoginCredentials : agre

        DodajPredmet --|> QDialog : nasl
        DodajPredmet --o NoviPredmet : agre
        DodajDomaci --|> QDialog : nasl
        DodajDomaci --o NoviDomaci : agre
        dodaj_ispit --|> QDialog : nasl
        dodaj_ispit --o NoviIspit : agre

        IspitTableModel --|> KorisnikQSqlRelationalTableModel : nasl
        DomaciTableModel --|> KorisnikQSqlRelationalTableModel : nasl
        PredmetTableModel --|> KorisnikQSqlRelationalTableModel : nasl

        Database --|> QSqlDatabase
        Database --o User
        Database --o QuerryResult
        class QSqlRelationalTableModel{
        }

        class QMainWindow{
        }

        class QDialog{
        }
        
        class QSqlDatabase{
        }

        class QuerryResult{
            bool success
            QSqlQuery qrr
            QString err
        }

        class Database{
            -const char* SQL_SELECT_DOMACI
            -const char* SQL_DELETE_DOMACI
            -const char* SQL_SELECT_ISPITI
            -const char* SQL_DELETE_ISPITI
            -const char* SQL_SELECT_PREDMETI
            -const char* SQL_ADD_PREDMET
            -const char* SQL_DELETE_PREDMET
            -QSqlDatabase database
            -User currentUser
            +handleDeleteByID(QString tableToDeleteFrom, int id) QuerryResult
            Database();

            getCurrentUser() *User
            getDatabase() *QSqlDatabase
            attemptUserAuth(QString username, QString password) QuerryResult
            getUserIspiti(int userID) QuerryResult
            getUserDomaci(int userID) QuerryResult
            getPredmeti() QuerryResult
            addDomaci(QString predmet, QString opis, QDate rok) QuerryResult
            addIspit(QString predmet, QString opis, QDate rok) QuerryResult
            addPredmet(QString akronim, QString naziv, int semestar, QString profesor, QString asistent) QuerryResult
            deleteDomaci(int id) QuerryResult
            deleteIspit(int id) QuerryResult
            deletePredmet(int id) QuerryResult
            getLastExecutedQuery(const QSqlQuery& query) QString
            connect(QString databaseType, QString ip, QString databaseName, QString username, QString password, int port=3306) bool
            close() void
        }

        class NoviIspit{
            +QString predmet
            +QString opis
            +QDate rok
        }

        class dodaj_ispit{
            -Ui::dodaj_ispit *ui
            +QString id
            +QString predmet
            +QString opis
            +QString datum
            +getData() noviIspit*
            -on_dodaj_buttonBox_accepted() void
            
        }

        class NoviDomaci{
            +QString predmet
            +QString opis
            +QDate rok
        }

        class DodajDomaci{
            -Ui::DodajDomaci *ui
            explicit DodajDomaci(QWidget *parent = nullptr);
            ~DodajDomaci();
            getData() NoviDomaci*
            on_buttonBox_accepted() void
        }

        class NoviPredmet{
            +QString akronim
            +QString naziv
            +int semestar
            +QString profesor
            +QString asistent
        }

        class DodajPredmet{
            -Ui::DodajPredmet *ui
            +explicit DodajPredmet(QWidget *parent = nullptr);
            +~DodajPredmet();
            +getData() NoviPredmet*
            +on_finish_pushButton_clicked() void
            +on_cancel_pushButton_clicked() void
        }

        class LoginCredentials{
            +QString username
            +QString password
        }

        class LoginForm{
            -Ui::LoginForm *ui
            +explicit LoginForm(QWidget *parent = nullptr);
            +ShowLogin() void
            +~LoginForm();
            +loginAttempt(QWidget *loginForm, LoginCredentials *creds) void
            +on_loginButton_clicked() void
        }

        class DomaciTableModel{
            +DomaciTableModel(QObject *parent, QString table, int korisnikid);
            +~DomaciTableModel();
        }

        class IspitTableModel{
            +IspitTableModel(QObject *parent, QString table, int korisnikid);
            +~IspitTableModel();
        }

        class PredmetTableModel{
            +PredmetTableModel(QObject *parent, QString table, int korisnikid);
            +~PredmetTableModel();
        }

        class KorisnikQSqlRelationalTableModel{
            -int korisnikid;
            +KorisnikQSqlRelationalTableModel(QObject *parent, QString table, int korisnikid)
            +~KorisnikQSqlRelationalTableModel()
            +setKorisnikID(int id) void
        }

        class User{
            -int id
            -QString username
            +setActiveUser() void
            +getUserID() int
            +getUsername() QString
        }

        class MainWindow{
            +Database database

            +MainWindow(QWidget *parent = nullptr, LoginForm *form=nullptr);
            +~MainWindow();
            +populateIspitiTableView() void
            +populateDomaciTabelView() void
            +populatePredmetiTableView() void
            +connectToDatabase() bool
            +addIspit(noviIspit *ispit) bool
            +addDomaci(NoviDomaci *domaci) bool
            +addPredmet(NoviPredmet *predmet) bool
            +connectTableViewContextMenus() void
            +contextDeleteRow(QString tableToUse, int dataEntryID) void
            +refreshAllTables() void
            +resizeTableViews() void
            +customMenuRequested(QPoint pos, QString tableToUse) void
            +customMenuRequested_Domaci(QPoint pos) void
            +customMenuRequested_Ispiti(QPoint pos) void
            +customMenuRequested_Predmeti(QPoint pos) void
            -on_dodajIspit_pushButton_clicked() void
            -on_dodajDomaci_pushButton_clicked() void
            -on_dodajPredmet_pushButton_clicked() void
            -on_osveziIspite_pushButton_clicked() void
            -on_osveziDomace_pushButton_clicked() void
            -on_osveziPredmete_pushButton_clicked() void
            -onLoginAttempt(QWidget *form, LoginCredentials *creds) void
        }
```

# uni-god2-oop2-student

    Aplikacija namenjena za olaksavanje vodjenja evidencije o neispunjenim obavezama i domacim zadatcima za studente

# Pokretanje baze
    

# Kompajliranje
    Udjete u root direktorijum projekta i onda:
        $ qmake
        $ make

# BITNE NAPOMENE
    1: Projekat u trenutnom stanju zajedno sa glavnom aplikacijom pokrece i konzolu, Ovo je namerna funkcionalnost radi Debugovanja ( qDebug() ),
    Ovo se moze ukloniti uklanjanjem parametra "console" u CONFIG-u

# Prezentacija

# Info
    Napravljeno koristeci QTCreator; Kompajlirano koristeci qmake na Manjaro Linux x86_64 sa parametrima navedenim u .pro fajlu



